#!/usr/bin/python
# -*- coding: utf-8 -*-

from ansible.module_utils.basic import AnsibleModule
import hvac


def main():
    module = AnsibleModule(
        argument_spec={
            'mount_point': {'type': 'str', 'default': 'approle'},
            'name': {'type': 'str', 'required': True},
            'url': {'type': 'str', 'required': True},
            'wrap_ttl': {'type': 'str'},
        },
    )

    client = hvac.Client(
        url=module.params['url'],
    )

    role_id = client.read("auth/{mount_point}/role/{name}/role-id".format(**module.params))

    secret_kwargs = {}
    if module.params['wrap_ttl'] is not None:
        secret_kwargs['wrap_ttl'] = module.params['wrap_ttl']
    secret_id = client.write("auth/{mount_point}/role/{name}/secret-id".format(**module.params), **secret_kwargs)

    module.exit_json(changed=True, role_id=role_id['data']['role_id'], secret_id=secret_id['wrap_info']['token'])

if __name__ == '__main__':
    main()
